<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(){
        return view('welco');
    }

    public function welcome_post(Request $request){
        return view('welco', ["nama_depan" => $request["nama_depan"], "nama_belakang" => $request["nama_belakang"]]);
    }
}
