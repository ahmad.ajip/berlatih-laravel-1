<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <form action="/welcome" method="POST">
        <h1>Buat Account Baru !</h1>
        <h3>Sign Up Form</h3>
        @csrf
        <label>First name:</label><br>
        <input type="text" name="nama_depan"><br>
        <br>
        <label>Last name:</label><br>
        <input type="text" name="nama_belakang"><br>
        <br>
        <label>Gender:</label><br>
        <input type="radio" name="gender_user" value="1">Male<br>
        <input type="radio" name="gender_user" value="2">Female<br>
        <input type="radio" name="gender_user" value="3">Other<br>
        <br>
        <label>Nationality:</label><br>
        <select>
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select><br>
        <br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="language_spoken" value="1">Bahasa Indonesia<br>
        <input type="checkbox" name="language_spoken" value="2">English<br>
        <input type="checkbox" name="language_spoken" value="3">Other<br>
        <br>
        <label>Bio:</label><br>
        <textarea cols=”30” rows=”30”></textarea><br>
        <br>
        <input type="submit" value="sign up">
    </form>
</body>
</html>
